# jira-api-utils

Set of python libraries and functions to programatically interact and manipulate JIRA projects and issues

It is foreseen to offer a SWAN instance for this. Stay tuned in Discourse!

In the meantime...

1. Download JIRA Python API from  https://pypi.org/project/jira/
2. Create a service account and log in once with it to JIRA
3. Contact the JIRA service managers via a SNOW ticket and explain them you would like to access JIRA through the API with the service account you have just created
4. Download the jira-api command and all the jira-*.py libraries
5. Make sure you define the necessary paths in your PYTHONPATH to access both the JIRA API and the libraries you have just downloaded