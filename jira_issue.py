#!/usr/bin/python

import jira_connect


# REST API for a particular issue: https://its.cern.ch/jira/rest/api/2/issue/PROJECTALT-11

def print_issue (issue):

   jira = jira_connect.get_jira_client()
   print "Information about Issue %s" % (issue)
   issue = jira.issue(issue)
   print "Summary: %s" % (issue.fields.summary)
   print "Last Updated: %s" % (issue.fields.updated)
   print "Assignee: %s" % (issue.fields.assignee.displayName)

